terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.0"
    }
  }
}

provider "azurerm" {
  features {}
  // Optional kann die Subscription angegeben werden, sofern mehrere vorhanden sind
  #subscription_id = "49f8bef1-f465-4dcc-b778-091e31bfd0b0"
}



resource "azurerm_resource_group" "rgdemo" {
  name     = "cla-tf-demo1"
  location = "West Europe"
}

resource "azurerm_virtual_network" "main" {
  name                = "vnet-cla-tf-demo1"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rgdemo.location
  resource_group_name = azurerm_resource_group.rgdemo.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rgdemo.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_interface" "main" {
  name                = "cla-tf-demo1-nic"
  location            = azurerm_resource_group.rgdemo.location
  resource_group_name = azurerm_resource_group.rgdemo.name

  ip_configuration {
    name                          = "ifconfig"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_windows_virtual_machine" "vmdemo" {
  name                  = "cla-tf-demo1"
  resource_group_name   = azurerm_resource_group.rgdemo.name
  location              = azurerm_resource_group.rgdemo.location
  size                  = "Standard_F2"
  admin_username        = "adminuser"
  admin_password        = "P@$$w0rd1234!"
  patch_assessment_mode = "AutomaticByPlatform"

  identity {
    type = "SystemAssigned"
  }

  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
    name                 = "cla-tf-demo1-disk0"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2022-Datacenter"
    version   = "latest"
  }
}
